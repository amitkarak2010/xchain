package com.xchain.commons.api.core.exceptions;


public class RestApiException extends Exception {

    private static final long serialVersionUID = -7635095387793482726L;

    public RestApiException() {
        super();
    }

    public RestApiException(String message,
            Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RestApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public RestApiException(String message) {
        super(message);
    }

    public RestApiException(Throwable cause) {
        super(cause);
    }

}
