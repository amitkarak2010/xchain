package com.xchain.commons.api.core.exceptions;


public class BadRequestException extends RestApiException {

    private static final long serialVersionUID = 7410194514693615704L;

    public BadRequestException() {
        super();
    }

    public BadRequestException(String message,
            Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }

}
