package com.xchain.commons.api.core.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintViolation;

public class ValidationUtils {

    public static Map<String, String> toMap(
            Collection<ConstraintViolation<?>> violations) {

        Map<String, String> errors = new HashMap<String, String>();

        for (ConstraintViolation<?> violation : violations) {
            errors.put(
                    violation.getPropertyPath().toString(),
                    violation.getMessage());
        }

        return errors;
    }
}
