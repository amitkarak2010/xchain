package com.xchain.commons.api.core.utils;

import java.util.Map;

import javax.validation.ConstraintViolationException;

import com.xchain.commons.api.core.model.ErrorResponse;

public class ErrorResponseUtils {

    public static ErrorResponse fromException(ConstraintViolationException e) {
        Map<String, String> errors = ValidationUtils.toMap(e.getConstraintViolations());
        return new ErrorResponse(errors);
    }

    public static ErrorResponse fromException(Exception e) {
        return new ErrorResponse(e.getMessage());
    }

}
