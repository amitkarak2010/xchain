package com.xchain.commons.api.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.xchain.commons.api.core.exceptions.BadRequestException;
import com.xchain.commons.api.core.exceptions.NotFoundException;
import com.xchain.commons.api.core.exceptions.RestApiException;
import com.xchain.commons.api.core.model.ErrorResponse;
import com.xchain.commons.api.core.utils.ErrorResponseUtils;

public abstract class AbstractEndpoint {

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractEndpoint.class);

    @ExceptionHandler(HttpMessageNotReadableException.class)
    protected ResponseEntity<Void> handleHttpMessageNotReadableException(
            HttpMessageNotReadableException ex) {
        LOG.debug("Caught HttpMessageNotReadableException", ex);
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<Void> handleBadRequestException(
            BadRequestException ex) {
        LOG.debug("Caught BadRequestException", ex);
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({RestApiException.class})
    protected ResponseEntity<ErrorResponse> handleRestApiException(RestApiException ex) {
        LOG.info("Caught RestApiException:" + ex.getMessage());
        ErrorResponse errorResponse = ErrorResponseUtils.fromException(ex);
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler({NotFoundException.class})
    protected ResponseEntity<Void> handleNotFoundException(NotFoundException ex) {
        LOG.info("Caught NotFoundException:" + ex.getMessage());
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    private Throwable getRootCause(Throwable e) {
        if (e.getCause() == null) {
            return e;
        }

        return getRootCause(e.getCause());
    }

}
