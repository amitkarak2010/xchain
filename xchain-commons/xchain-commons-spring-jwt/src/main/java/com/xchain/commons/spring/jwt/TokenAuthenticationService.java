package com.xchain.commons.spring.jwt;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.xchain.commons.spring.jwt.config.AuthProperties;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthenticationService.class);

    private final AuthProperties authProperties;

    public TokenAuthenticationService(AuthProperties authProperties) {
        this.authProperties = authProperties;
    }

    public String generateToken(String username, String[] roles) {

        String jwt = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(username)
                .claim("roles", roles)
                .signWith(SignatureAlgorithm.HS512, authProperties.getSecret()).compact();

        return jwt;

    }

    public Authentication getAuthentication(HttpServletRequest request) {
        try {
            String token = request.getHeader(authProperties.getHeader());

            if (token != null) {
                String jwt = token.replace(authProperties.getTokenPrefix(), "");

                Claims claims = Jwts.parser()
                        .setSigningKey(authProperties.getSecret())
                        .parseClaimsJws(jwt)
                        .getBody();

                String username = claims.getSubject();
                @SuppressWarnings("unchecked")
                List<String> roles = (List<String>) claims.get("roles");

                return buildAuthentication(username, roles);

            }
        } catch (JwtException e) {
            LOG.info(e.getMessage());
        } catch (Exception e) {
            LOG.info("Error validating JWT", e);
        }
        return null;
    }

    private Authentication buildAuthentication(String username, List<String> roles) {
        if (username != null) {
            UserDetails userDetails = User.withUsername(username)
                    .password(randomPassword())
                    .roles(toRoleStrings(roles))
                    .build();

            return new UsernamePasswordAuthenticationToken(
                    userDetails.getUsername(),
                    userDetails.getPassword(),
                    userDetails.getAuthorities());
        } else {
            LOG.info("Invalid JWT token, username is null");
            return null;
        }
    }

    private String[] toRoleStrings(List<String> roles) {
        if (roles != null) {
            return roles.stream().toArray(String[]::new);
        }

        return new String[0];
    }

    private String randomPassword() {
        return UUID.randomUUID().toString();
    }

}
