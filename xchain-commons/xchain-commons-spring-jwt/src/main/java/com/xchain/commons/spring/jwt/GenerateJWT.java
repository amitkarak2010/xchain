package com.xchain.commons.spring.jwt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

import com.xchain.commons.spring.jwt.config.AuthProperties;

public class GenerateJWT {

    private static final Logger LOG = LoggerFactory.getLogger(GenerateJWT.class);


    public static String generateToken(String username, String[] roles)
            throws FileNotFoundException {
        LOG.info(String.format("Generating JTW: username: %s roles: %s", username,
                Arrays.toString(roles)));
        AuthProperties authProperties = loadAuthProperties("config/application.yml");
        TokenAuthenticationService service = new TokenAuthenticationService(authProperties);
        String token = service.generateToken(username, roles);
        System.out.println(token);
        return token;
    }

    private static AuthProperties loadAuthProperties(String filename) throws FileNotFoundException {

        Properties properties = loadProperties(filename);
        String secret = properties.getProperty("restApi.auth.secret");

        AuthProperties authProperties = new AuthProperties();
        authProperties.setSecret(secret);

        return authProperties;
    }

    private static Properties loadProperties(String filename) throws FileNotFoundException {
        InputStream in = new FileInputStream(filename);
        Resource resource = new InputStreamResource(in);
        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
        factory.setResources(resource);

        return factory.getObject();
    }

    public static void printHelp(Options options) {
        final HelpFormatter formatter = new HelpFormatter();
        final String syntax = "generate_jwt --username <username> --roles ROLE1 ROLE2 ...";
        final String usageHeader = "where options include:";

        formatter.printHelp(syntax, usageHeader, options, null);
    }

}
