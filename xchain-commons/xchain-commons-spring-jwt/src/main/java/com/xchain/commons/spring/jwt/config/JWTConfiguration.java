package com.xchain.commons.spring.jwt.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xchain.commons.spring.jwt")
public class JWTConfiguration {

}
