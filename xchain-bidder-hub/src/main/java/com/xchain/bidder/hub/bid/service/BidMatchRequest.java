package com.xchain.bidder.hub.bid.service;

import java.util.Map;

import org.springframework.data.domain.Pageable;

import lombok.Data;

@Data
public class BidMatchRequest {

	private Map<String, String> filter;
	private Pageable pageable;
	
	public BidMatchRequest withFilter(Map<String, String> filter) {
		this.filter = filter;
		return this;
	}
	
	public BidMatchRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
