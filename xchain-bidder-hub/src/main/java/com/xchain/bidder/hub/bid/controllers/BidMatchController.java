package com.xchain.bidder.hub.bid.controllers;

import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bid.service.BidMatchRequest;
import com.xchain.bidder.hub.bid.service.BidMatchService;
import com.xchain.bidder.hub.models.BidPageDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class BidMatchController extends AbstractEndpoint {

	private final BidMatchService bidMatchService;
	
	public BidMatchController(BidMatchService bidMatchService) {
		this.bidMatchService = bidMatchService;
	}
	
    @RequestMapping(
            value = "/bid/match",
            method = RequestMethod.GET)
    public BidPageDTO handleRequest(@RequestParam Map<String, String> filter,
            @SortDefault.SortDefaults({
                @SortDefault(sort = "bidPrice", direction = Direction.DESC)
            }) Pageable pageable) throws Exception {
    	
    	return bidMatchService.execute(new BidMatchRequest()
    			.withFilter(filter).withPageable(pageable));
    }
}
