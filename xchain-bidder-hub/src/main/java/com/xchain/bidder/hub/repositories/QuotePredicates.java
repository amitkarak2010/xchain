package com.xchain.bidder.hub.repositories;

import java.util.Date;
import java.util.UUID;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.xchain.bidder.hub.entities.QQuote;
import com.xchain.bidder.hub.utils.QuoteStatus;

public class QuotePredicates {

	public static BooleanExpression withQuoteOwner(UUID quoteOwnerId) {
		QQuote quote = QQuote.quote;
		if(quoteOwnerId == null) {
			return null;
		}
		return quote.quoteOwnerId.eq(quoteOwnerId).and(
				quote.quoteStatus.eq(QuoteStatus.ACTIVE.toString()));
	}
	
    public static BooleanExpression withExpirableQuotes() {
        QQuote quote = QQuote.quote;
        Date now = new Date();

        return quote.expireAt.before(now).and(quote.quoteStatus.eq(
        		QuoteStatus.ACTIVE.toString()));
    }
}
