package com.xchain.bidder.hub.mappings;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.bid.service.BidCreateRequest;
import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.bidder.hub.models.BidPageDTO;

@Service
public class BidMappingService extends AbstractMappingSerice {

	public BidDTO map(Bid source) {
		return this.modelMapper.map(source, BidDTO.class);
	}
	
	public Bid map(BidCreateRequest request) {
		return this.modelMapper.map(request, Bid.class);
	}
	
	public BidPageDTO map(Page<Bid> source) {
		return this.modelMapper.map(source, BidPageDTO.class);
	}
}
