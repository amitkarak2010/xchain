package com.xchain.bidder.hub.models;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class QuoteDTO extends AbstractModel {

	private String quoteName;
	private UUID quoteOwnerId;
	private UUID cartId;
	private Double quotePrice;
	private Double basePrice;
	private String quoteStatus;
	private Date expireAt;
	private Set<BidderDTO> bids;
}
