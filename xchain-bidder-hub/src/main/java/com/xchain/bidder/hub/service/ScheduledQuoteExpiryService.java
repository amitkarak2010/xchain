package com.xchain.bidder.hub.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.repositories.QuoteRepository;
import com.xchain.bidder.hub.utils.QuoteStatus;

import static com.xchain.bidder.hub.repositories.QuotePredicates.withExpirableQuotes;

import java.util.Iterator;

@Service
public class ScheduledQuoteExpiryService {

    private static final Logger LOG = LoggerFactory.getLogger(ScheduledQuoteExpiryService.class);

    private final QuoteRepository quoteRepository;

    public ScheduledQuoteExpiryService(QuoteRepository quoteRepository) {
        this.quoteRepository = quoteRepository;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void expireQuotes() {
        Iterable<Quote> iterable = quoteRepository.findAll(withExpirableQuotes());
        Iterator<Quote> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Quote quote = iterator.next();
            try {
            	quote.setQuoteStatus(QuoteStatus.EXPIRED.toString());
                quoteRepository.save(quote);
            } catch (Exception e) {
                LOG.error("Encountered unexpected error when trying to expire quote: "
                        + quote.getId(), e);
            }
        }
    }

    @Scheduled(cron = "${expire-quotes.schedule}")
    public void scheduled() {
        LOG.info("Running scheduled quote expiry job");
        expireQuotes();
    }
}
