package com.xchain.bidder.hub.bid.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bid.service.BidUpdateRequest;
import com.xchain.bidder.hub.bid.service.BidUpdateService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.BadRequestException;


@RestController
public class BidUpdateController extends AbstractEndpoint {

	private final BidUpdateService bidUpdateService;
	
	public BidUpdateController(BidUpdateService bidUpdateService) {
		this.bidUpdateService = bidUpdateService;
	}
	
	@RequestMapping(
            value = "/bid/update",
            method = RequestMethod.POST)
	public BidDTO createBid(@RequestBody BidUpdateRequest request) throws Exception {
		if(validateRequest(request)) {
			BidDTO bid = bidUpdateService.execute(request);
			return bid;
		} else {
			throw new BadRequestException("Bad bid update request");
		}
	}
	
	private Boolean validateRequest(BidUpdateRequest request) {
		if(request.getBidPrice() == null) {
			LOG.error("Bad bid update request, bid price can not be null for a bid");
			return false;
		}
		return true;
	}
}
