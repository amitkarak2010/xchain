package com.xchain.bidder.hub.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.xchain.bidder.hub.entities.Cart;

public interface CartRespository extends 
					JpaRepository<Cart, UUID>, QueryDslPredicateExecutor<Cart> {

}
