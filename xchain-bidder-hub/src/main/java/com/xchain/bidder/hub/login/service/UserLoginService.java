package com.xchain.bidder.hub.login.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.entities.UserProfile;
import com.xchain.bidder.hub.mappings.UserLoginMappingService;
import com.xchain.bidder.hub.models.UserLoginDTO;
import com.xchain.bidder.hub.repositories.BidderRepository;
import com.xchain.bidder.hub.repositories.UserProfileRepository;
import com.xchain.bidder.hub.utils.PasswordService;
import com.xchain.commons.api.core.exceptions.BadRequestException;
import com.xchain.commons.api.core.exceptions.NotFoundException;
import com.xchain.commons.spring.jwt.GenerateJWT;

import static com.xchain.bidder.hub.repositories.UserPredicates.withBidderEmail;
import static com.xchain.bidder.hub.repositories.UserPredicates.withUserEmail;

@Service
public class UserLoginService {

	private final UserProfileRepository userProfileRepository;
	private final BidderRepository bidderRepository;
	private final PasswordService passwordService;
	private final UserLoginMappingService userLoginMappingService;
	
	public UserLoginService(UserProfileRepository userProfileRepository,
			BidderRepository bidderRepository,
			PasswordService passwordService,
			UserLoginMappingService userLoginMappingService){
		this.userProfileRepository = userProfileRepository;
		this.bidderRepository = bidderRepository;
		this.passwordService = passwordService;
		this.userLoginMappingService = userLoginMappingService;
	}
	
	
	/**
	 * the method should get divided in multiple services.
	 * For time being putting all code at one place and
	 * violating the segregation principle for time being.
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public UserLoginDTO execute(UserLoginRequest request) throws Exception {
		Predicate emailPredicate = null;
		UserLoginDTO userLoginDTO = null;
		if(request.getAccountType().equalsIgnoreCase("BIDDER")) {
			emailPredicate = withBidderEmail(request.getEmail());
			Bidder bidder = bidderRepository.findOne(emailPredicate);
			if(bidder == null) {
				throw new NotFoundException("Bidder with email : " + request.getEmail()
												+ " not found inside the system.");
			}
			boolean isValidUser = passwordService.matchPassword(request.getPassword(), 
					bidder.getPassword());
			if(isValidUser) {
				userLoginDTO = userLoginMappingService.map(bidder);
				userLoginDTO.setToken(GenerateJWT.generateToken(request.getAccountType(), 
						new String[] {"API"}));
				userLoginDTO.setAccountType(request.getAccountType());
			} else {
				throw new BadRequestException("Password passed for the "
						+ "bidder " + request.getEmail() + " does not match");
			}
			return userLoginDTO;
		} else if(request.getAccountType().equalsIgnoreCase("USER")) {
			emailPredicate = withUserEmail(request.getEmail());
			UserProfile userProfile = userProfileRepository.findOne(emailPredicate);
			if(userProfile == null) {
				throw new NotFoundException("Bidder with email : " + request.getEmail()
												+ " not found inside the system.");
			}
			boolean isValidUser = passwordService.matchPassword(request.getPassword(), 
					userProfile.getPassword());
			if(isValidUser) {
				userLoginDTO = userLoginMappingService.map(userProfile);
				userLoginDTO.setToken(GenerateJWT.generateToken(request.getAccountType(), 
						new String[] {"API"}));
				userLoginDTO.setAccountType(request.getAccountType());
			} else {
				throw new BadRequestException("Password passed for the "
						+ "user " + request.getEmail() + " does not match");
			}
			return userLoginDTO;
		}
		return null;
	}
}
