package com.xchain.bidder.hub.bidder.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.mappings.BidderMappingService;
import com.xchain.bidder.hub.repositories.BidderRepository;
import com.xchain.bidder.hub.utils.PasswordService;

@Service
public class BidderCreateService {
	private final BidderRepository bidderRepository;
	private final BidderMappingService bidderMappingService;
	private final PasswordService passwordService;
	
	public BidderCreateService(BidderRepository bidderRepository,
			BidderMappingService bidderMappingService,
			PasswordService passwordService) {
		this.bidderRepository = bidderRepository;
		this.bidderMappingService = bidderMappingService;
		this.passwordService = passwordService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public Bidder execute(BidderCreateRequest request) throws Exception {
		Bidder bidder = bidderMappingService.map(request);
		String password = passwordService.encodePassword(request.getPassword());
		bidder.setPassword(password);
		bidder = bidderRepository.save(bidder);
		return bidder;
	}
}
