package com.xchain.bidder.hub.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "commodity")
public class Commodity extends AbstractEntity {

	private static final long serialVersionUID = -9219878131154468924L;

	@Column(name = "commodity_name")
	private String commodityName;
	
	@Column(name = "commodity_description")
	private String commodityDescription;
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "category")
	private String category;

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public String getCommodityDescription() {
		return commodityDescription;
	}

	public void setCommodityDescription(String commodityDescription) {
		this.commodityDescription = commodityDescription;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
