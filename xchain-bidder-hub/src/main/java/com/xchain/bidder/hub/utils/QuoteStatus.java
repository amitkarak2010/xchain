package com.xchain.bidder.hub.utils;

import org.apache.commons.lang3.StringUtils;

public enum QuoteStatus {

	DRAFT("DRAFT"), 
	ACTIVE("ACTIVE"), 
	FAILED("FAILED"), 
	EXPIRED("EXPIRED");

	private String description;

	QuoteStatus(String description) {
    	this.description = description;
    }

	public static QuoteStatus fromName(String name) {
		if (!StringUtils.isEmpty(name)) {
			name = name.trim();
			QuoteStatus[] quoteStatus = QuoteStatus.values();
			for (QuoteStatus status : quoteStatus) {
				if (status.name().equalsIgnoreCase(name) 
						|| status.description.equalsIgnoreCase(name)) {
					return status;
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.description;
	}	
}
