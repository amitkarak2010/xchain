package com.xchain.bidder.hub.entities;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "quote")
public class Quote extends AbstractEntity {

	private static final long serialVersionUID = -3472577383442741353L;

	@Column(name = "quote_name")
	private String quoteName;
	
	@Column(name = "quote_owner_id")
	private UUID quoteOwnerId;
	
	@Column(name = "cart_id")
	private UUID cartId;
	
	@Column(name = "quote_price")
	private Double quotePrice;
	
	@Column(name = "base_price")
	private Double basePrice;
	
	@Column(name = "quote_status")
	private String quoteStatus;
	
	@Column(name = "expire_at")
	private Date expireAt;
	
    @OneToMany(
            mappedBy = "quote",
            targetEntity = Bid.class)
	private Set<Bid> bids;

	public String getQuoteName() {
		return quoteName;
	}

	public void setQuoteName(String quoteName) {
		this.quoteName = quoteName;
	}

	public UUID getQuoteOwnerId() {
		return quoteOwnerId;
	}

	public void setQuoteOwnerId(UUID quoteOwnerId) {
		this.quoteOwnerId = quoteOwnerId;
	}

	public UUID getCartId() {
		return cartId;
	}

	public void setCartId(UUID cartId) {
		this.cartId = cartId;
	}

	public Double getQuotePrice() {
		return quotePrice;
	}

	public void setQuotePrice(Double quotePrice) {
		this.quotePrice = quotePrice;
	}

	public String getQuoteStatus() {
		return quoteStatus;
	}

	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	public Set<Bid> getBids() {
		return bids;
	}

	public void setBids(Set<Bid> bids) {
		this.bids = bids;
	}

	public Double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}
}
