package com.xchain.bidder.hub.bidder.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bidder.service.BidderCreateRequest;
import com.xchain.bidder.hub.bidder.service.BidderCreateService;
import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.mappings.BidderMappingService;
import com.xchain.bidder.hub.models.BidderDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class BidderCreateController extends AbstractEndpoint {
	
	private final BidderCreateService bidderCreateService;
	private final BidderMappingService bidderMappingService;
	
	public BidderCreateController(BidderCreateService bidderCreateService,
									BidderMappingService bidderMappingService) {
		this.bidderCreateService = bidderCreateService;
		this.bidderMappingService = bidderMappingService;
	}

	@RequestMapping(
            value = "/admin/-/bidder",
            method = RequestMethod.POST)
	public BidderDTO createBidder(@RequestBody BidderCreateRequest request) throws Exception {
		if(validateRequest(request)) {
	    	Bidder bidder = bidderCreateService.execute(request);
	    	if(bidder == null) {
	    		throw new RestApiException("Unable to create user profile");
	    	}
	    	return bidderMappingService.map(bidder);
		} 
		return null;
	}
	
	private boolean validateRequest(BidderCreateRequest request) throws Exception {
		if(StringUtils.isBlank(request.getEmail())) {
			throw new RestApiException("Bad request, email is null");
		} else if(StringUtils.isBlank(request.getPassword())) {
			throw new RestApiException("Bad request, password is null");
		}
		return true;
	}
}
