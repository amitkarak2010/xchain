package com.xchain.bidder.hub.repositories;

import java.util.UUID;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.xchain.bidder.hub.entities.QBid;

public class BidPredicates {

	public static BooleanExpression withBidOwner(UUID bidOwnerId) {
		QBid bid = QBid.bid;
		if(bidOwnerId == null) {
			return null;
		}
		return bid.bidOwnerId.eq(bidOwnerId);
	}
	
	public static BooleanExpression withQuoteId(UUID quoteId) {
		QBid bid = QBid.bid;
		if(quoteId == null) {
			return null;
		}
		return bid.quote.id.eq(quoteId);
	}
}
