package com.xchain.bidder.hub.quote.controllers;

import java.util.UUID;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.quote.service.QuoteActivateService;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class QuoteActivateController extends AbstractEndpoint {

	private final QuoteActivateService quoteActivateService;
	
	public QuoteActivateController(QuoteActivateService quoteActivateService) {
		this.quoteActivateService = quoteActivateService;
	}
	
	@RequestMapping(
            value = "/quote/{quoteId}/activate",
            method = RequestMethod.POST)
	public QuoteDTO activateQuote(@PathVariable UUID quoteId) throws Exception {
		return quoteActivateService.activateQuote(quoteId);
	}
}
