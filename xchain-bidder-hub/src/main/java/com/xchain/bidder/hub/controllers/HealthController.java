package com.xchain.bidder.hub.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class HealthController extends AbstractEndpoint {

    @RequestMapping(
            value = "/health",
            method = RequestMethod.GET)
    public ResponseEntity<Void> healthCheck() {
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}