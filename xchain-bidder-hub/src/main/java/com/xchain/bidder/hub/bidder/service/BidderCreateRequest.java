package com.xchain.bidder.hub.bidder.service;

import com.xchain.bidder.hub.models.BidderDTO;

public class BidderCreateRequest extends BidderDTO {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
