package com.xchain.bidder.hub.cart.controllers;

import java.util.UUID;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.cart.service.CartGetRequest;
import com.xchain.bidder.hub.cart.service.CartGetService;
import com.xchain.bidder.hub.models.CartDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class CartGetController extends AbstractEndpoint {

	private final CartGetService cartGetService;
	
	public CartGetController(CartGetService cartGetService) {
		this.cartGetService = cartGetService;
	}
	
    @RequestMapping(
            value = "/cart/{cartId}",
            method = RequestMethod.GET)
    public CartDTO getCart(@PathVariable UUID cartId) throws Exception {
    	return cartGetService.execute(new CartGetRequest().withCartId(cartId));
    }
}
