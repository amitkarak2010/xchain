package com.xchain.bidder.hub.bid.controllers;

import java.util.UUID;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bid.service.BidGetRequest;
import com.xchain.bidder.hub.bid.service.BidGetService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class BidGetController extends AbstractEndpoint {

	private final BidGetService bidGetService;

	public BidGetController(BidGetService bidGetService) {
		this.bidGetService = bidGetService;
	}

	@RequestMapping(value = "/bid/{bidId}", 
			method = RequestMethod.GET)
	public BidDTO getBid(@PathVariable UUID bidId) throws Exception {
		return bidGetService.execute(new BidGetRequest().withBidId(bidId));
	}
}
