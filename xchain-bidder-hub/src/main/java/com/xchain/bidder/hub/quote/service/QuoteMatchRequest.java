package com.xchain.bidder.hub.quote.service;

import java.util.Map;

import org.springframework.data.domain.Pageable;

import lombok.Data;

@Data
public class QuoteMatchRequest {

	private Map<String, String> filter;
	private Pageable pageable;
	
	public QuoteMatchRequest withFilter(Map<String, String> filter) {
		this.filter = filter;
		return this;
	}
	
	public QuoteMatchRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
