package com.xchain.bidder.hub.commodity.service;

import org.springframework.data.domain.Pageable;

import lombok.Data;

@Data
public class CommodityListRequest {

	private Pageable pageable;
	
	public CommodityListRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
