package com.xchain.bidder.hub.login.service;

import com.xchain.bidder.hub.models.UserLoginDTO;

import lombok.Data;

@Data
public class UserLoginRequest extends UserLoginDTO {
	
	private String password;

}
