package com.xchain.bidder.hub.models;

import java.util.UUID;

import lombok.Data;

@Data
public class UserLoginDTO {

	private String userName;
	private UUID userId;
	private String token;
	private String email;
	private String accountType;
}
