package com.xchain.bidder.hub.bid.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bid.service.BidCreateRequest;
import com.xchain.bidder.hub.bid.service.BidCreateService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.BadRequestException;

@RestController
public class BidCreateController extends AbstractEndpoint {

	private final BidCreateService bidCreateService;
	
	public BidCreateController(BidCreateService bidCreateService) {
		this.bidCreateService = bidCreateService;
	}
	
	@RequestMapping(
            value = "/bid",
            method = RequestMethod.POST)
	public BidDTO createBid(@RequestBody BidCreateRequest request) throws Exception {
		if(validateRequest(request)) {
			BidDTO bid = bidCreateService.execute(request);
			return bid;
		} else {
			throw new BadRequestException("Bad bid create request");
		}
	}
	
	private Boolean validateRequest(BidCreateRequest request) {
		if(request.getBidOwnerId() == null) {
			LOG.error("Bad bid create request, bid owner id can not be null for a bid");
			return false;
		} else if(request.getBidPrice() == null) {
			LOG.error("Bad bid create request, bid price can not be null for a bid");
			return false;
		}
		return true;
	}
}
