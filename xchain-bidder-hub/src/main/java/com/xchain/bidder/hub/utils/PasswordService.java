package com.xchain.bidder.hub.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public PasswordService() {
		this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
	}
	
	public String encodePassword(String password) {
		return bCryptPasswordEncoder.encode(password);
	}
	
	public boolean matchPassword(String password, String encodedPassword) {
		return bCryptPasswordEncoder.matches(password, encodedPassword);
	}
}
