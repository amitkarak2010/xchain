package com.xchain.bidder.hub.cart.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.cart.service.CartUpdateRequest;
import com.xchain.bidder.hub.cart.service.CartUpdateService;
import com.xchain.bidder.hub.models.CartDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.BadRequestException;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class CartUpdateController extends AbstractEndpoint {

	private final CartUpdateService cartUpdateService;
	
	public CartUpdateController(CartUpdateService cartUpdateService) {
		this.cartUpdateService = cartUpdateService;
	}

	@RequestMapping(
            value = "/cart/update",
            method = RequestMethod.POST)
	public CartDTO updateCart(@RequestBody CartUpdateRequest request) throws Exception {
		if(validateRequest(request)) {
	    	CartDTO cart = cartUpdateService.execute(request);
	    	if(cart == null) {
	    		throw new RestApiException("Unable to update cart with cart name : " 
	    										+request.getCartName());
	    	}
	    	return cart;
		} else {
			throw new BadRequestException("Cart id can not be null");
		}
	}

	private Boolean validateRequest(CartUpdateRequest request) {
		if(request.getId() == null) {
			return false;
		}
		return true;
	}
}
