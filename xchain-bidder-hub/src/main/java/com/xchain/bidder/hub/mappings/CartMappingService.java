package com.xchain.bidder.hub.mappings;

import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.cart.service.CartCreateRequest;
import com.xchain.bidder.hub.entities.Cart;
import com.xchain.bidder.hub.models.CartDTO;

@Service
public class CartMappingService extends AbstractMappingSerice {

	public CartDTO map(Cart source) {
		return this.modelMapper.map(source, CartDTO.class);
	}
	
	public Cart map(CartCreateRequest source) {
		return this.modelMapper.map(source, Cart.class);
	}
}
