package com.xchain.bidder.hub.bid.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.mappings.BidMappingService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.bidder.hub.repositories.BidRespository;
import com.xchain.commons.api.core.exceptions.NotFoundException;

@Service
public class BidUpdateService {

	private final BidMappingService bidMappingService;
	private final BidRespository bidRepository;
	
	public BidUpdateService(BidMappingService bidMappingService,
			BidRespository bidRepository) {
		this.bidMappingService = bidMappingService;
		this.bidRepository = bidRepository;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public BidDTO execute(BidUpdateRequest request) throws Exception {
		Bid bid = bidRepository.findOne(request.getId());
		if(bid == null) {
			throw new NotFoundException("No Bid found with bid id : " + request.getId());
		}
		bid.setBidPrice(request.getBidPrice());
		bid.setBidStatus(request.getBidStatus());
		bid = bidRepository.save(bid);
		return bidMappingService.map(bid);
	}
}
