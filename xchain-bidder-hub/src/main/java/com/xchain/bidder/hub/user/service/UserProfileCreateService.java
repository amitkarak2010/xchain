package com.xchain.bidder.hub.user.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.UserProfile;
import com.xchain.bidder.hub.mappings.UserProfileMappingService;
import com.xchain.bidder.hub.repositories.UserProfileRepository;
import com.xchain.bidder.hub.utils.PasswordService;

@Service
public class UserProfileCreateService {

	private final UserProfileRepository userProfileRepository;
	private final UserProfileMappingService userProfileMappingService;
	private final PasswordService passwordService;

	public UserProfileCreateService(UserProfileRepository userProfileRepository,
			UserProfileMappingService userProfileMappingService, PasswordService passwordService) {
		this.userProfileRepository = userProfileRepository;
		this.userProfileMappingService = userProfileMappingService;
		this.passwordService = passwordService;
	}

	@Transactional(isolation = Isolation.SERIALIZABLE)
	public UserProfile execute(UserProfileCreateRequest request) throws Exception {
		UserProfile userProfile = userProfileMappingService.map(request);
		String password = passwordService.encodePassword(request.getPassword());
		userProfile.setPassword(password);
		userProfile = userProfileRepository.save(userProfile);
		return userProfile;
	}
}
