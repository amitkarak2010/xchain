package com.xchain.bidder.hub.bidder.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.mappings.BidderMappingService;
import com.xchain.bidder.hub.models.BidderPageDTO;
import com.xchain.bidder.hub.repositories.BidderRepository;

@Service
public class BidderListService {

	private final BidderRepository bidderRepository;
	private final BidderMappingService bidderMappingService;
	
	public BidderListService(BidderRepository bidderRepository,
			BidderMappingService bidderMappingService) {
		this.bidderMappingService = bidderMappingService;
		this.bidderRepository = bidderRepository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BidderPageDTO execute(BidderListRequest request) {
		Page<Bidder> bidders = bidderRepository.findAll(request.getPageable());
		return bidderMappingService.map(bidders);
	}
}
