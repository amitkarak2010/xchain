package com.xchain.bidder.hub.bidder.controllers;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bidder.service.BidderListRequest;
import com.xchain.bidder.hub.bidder.service.BidderListService;
import com.xchain.bidder.hub.models.BidderPageDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class BidderListController extends AbstractEndpoint {

	private final BidderListService bidderListService;
	
	public BidderListController(BidderListService bidderListService) {
		this.bidderListService = bidderListService;
	}
	
    @RequestMapping(
            value = "/bidder/list",
            method = RequestMethod.GET)
    public BidderPageDTO listBidders(
    		@SortDefault.SortDefaults({
                @SortDefault(sort = "lastModified", direction = Direction.DESC)
            }) Pageable pageable) throws Exception {
    	
    	return bidderListService.execute(new BidderListRequest()
    			.withPageable(pageable));
    }
}
