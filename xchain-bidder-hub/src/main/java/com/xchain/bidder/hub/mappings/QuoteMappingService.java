package com.xchain.bidder.hub.mappings;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.models.QuotePageDTO;
import com.xchain.bidder.hub.quote.service.QuoteCreateRequest;

@Service
public class QuoteMappingService extends AbstractMappingSerice {

	public Quote map(QuoteCreateRequest request) {
		return this.modelMapper.map(request, Quote.class);
	}
	
	public QuoteDTO map(Quote source) {
		return this.modelMapper.map(source, QuoteDTO.class);
	}
	
	public QuotePageDTO map(Page<Quote> source) {
		return this.modelMapper.map(source, QuotePageDTO.class);
	}
}
