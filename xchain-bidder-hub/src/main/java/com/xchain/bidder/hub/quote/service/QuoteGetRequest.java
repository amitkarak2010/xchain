package com.xchain.bidder.hub.quote.service;

import java.util.UUID;

import lombok.Data;

@Data
public class QuoteGetRequest {

	private UUID quoteId;
	
	public QuoteGetRequest withQuoteId(UUID quoteId) {
		this.quoteId = quoteId;
		return this;
	}
}
