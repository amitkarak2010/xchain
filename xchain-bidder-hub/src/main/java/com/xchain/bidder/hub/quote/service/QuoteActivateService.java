package com.xchain.bidder.hub.quote.service;

import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.mappings.QuoteMappingService;
import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.repositories.QuoteRepository;
import com.xchain.bidder.hub.utils.QuoteStatus;
import com.xchain.commons.api.core.exceptions.NotFoundException;

@Service
public class QuoteActivateService {

	private final QuoteRepository quoteRepository;
	private final QuoteMappingService quoteMappingService;
	
	public QuoteActivateService(QuoteRepository quoteRepository,
			QuoteMappingService quoteMappingService) {
		this.quoteRepository = quoteRepository;
		this.quoteMappingService = quoteMappingService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public QuoteDTO activateQuote(UUID quoteId) throws Exception {
		Quote quote = quoteRepository.findOne(quoteId);
		if(quote == null) {
			throw new NotFoundException("Unable to activzte quote, "
					+ "No quote found with id: " + quoteId.toString());
		}
		quote.setQuoteStatus(QuoteStatus.ACTIVE.toString());
		quote = quoteRepository.save(quote);
		return quoteMappingService.map(quote);
	}
}
