package com.xchain.bidder.hub.mappings;

import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.entities.UserProfile;
import com.xchain.bidder.hub.models.UserLoginDTO;

@Service
public class UserLoginMappingService extends AbstractMappingSerice {
	
	public UserLoginDTO map(Bidder source) {
		return this.modelMapper.map(source, UserLoginDTO.class);
	}
	
	public UserLoginDTO map(UserProfile source) {
		return this.modelMapper.map(source, UserLoginDTO.class);
	}
}
