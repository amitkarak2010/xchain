package com.xchain.bidder.hub.repositories;

import org.apache.commons.lang3.StringUtils;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.xchain.bidder.hub.entities.QBidder;
import com.xchain.bidder.hub.entities.QUserProfile;

public class UserPredicates {

	public static BooleanExpression withBidderEmail(String email) {
		QBidder bidder = QBidder.bidder;
		if(StringUtils.isBlank(email)) {
			return null;
		}
		return bidder.email.eq(email);
	}

	public static BooleanExpression withUserEmail(String email) {
		QUserProfile userProfile = QUserProfile.userProfile;
		if(StringUtils.isBlank(email)) {
			return null;
		}
		return userProfile.email.eq(email);
	}
}
