package com.xchain.bidder.hub.models;

import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class UserProfileDTO extends AbstractModel {

	private String firstName;
	private String lastName;
	private String email;
	private String contactNumber;
	private String city;
	private String state;
	private String country;
	private String accountType;
}
