package com.xchain.bidder.hub.cart.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Cart;
import com.xchain.bidder.hub.mappings.CartMappingService;
import com.xchain.bidder.hub.mappings.CommodityMappingService;
import com.xchain.bidder.hub.models.CartDTO;
import com.xchain.bidder.hub.repositories.CartRespository;

@Service
public class CartUpdateService {

	private final CartMappingService cartMappingService;
	private final CartRespository cartRespository;
	private final CommodityMappingService commodityMappingService;
	
	public CartUpdateService(CartMappingService cartMappingService,
			CartRespository cartRespository,
			CommodityMappingService commodityMappingService) {
		this.cartMappingService = cartMappingService;
		this.cartRespository = cartRespository;
		this.commodityMappingService = commodityMappingService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public CartDTO execute(CartUpdateRequest request) throws Exception {
		Cart cart = cartRespository.findOne(request.getId());
		cart.setCommodity(commodityMappingService.map(request.getCommodityDTO()));
		cart = cartRespository.save(cart);
		return cartMappingService.map(cart);
	}
}
