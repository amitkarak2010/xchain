package com.xchain.bidder.hub.login.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.login.service.UserLoginRequest;
import com.xchain.bidder.hub.login.service.UserLoginService;
import com.xchain.bidder.hub.models.UserLoginDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class UserLoginController extends AbstractEndpoint {
	private final UserLoginService userLoginService;
	
	public UserLoginController(UserLoginService userLoginService) {
		this.userLoginService = userLoginService;
	}
	
	@RequestMapping(
            value = "/login",
            method = RequestMethod.POST)
    public UserLoginDTO login(@RequestBody UserLoginRequest request) throws Exception {
		if(validateRequest(request)) {
	    	return userLoginService.execute(request);
		} 
		return null;
    }
	
	private boolean validateRequest(UserLoginRequest request) throws Exception {
		if(StringUtils.isBlank(request.getEmail())) {
			throw new RestApiException("Bad request, email is null");
		} else if(StringUtils.isBlank(request.getPassword())) {
			throw new RestApiException("Bad request, password is null");
		}
		return true;
	}
}
