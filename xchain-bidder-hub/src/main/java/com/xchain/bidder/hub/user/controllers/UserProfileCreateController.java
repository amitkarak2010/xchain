package com.xchain.bidder.hub.user.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.entities.UserProfile;
import com.xchain.bidder.hub.mappings.UserProfileMappingService;
import com.xchain.bidder.hub.models.UserProfileDTO;
import com.xchain.bidder.hub.user.service.UserProfileCreateRequest;
import com.xchain.bidder.hub.user.service.UserProfileCreateService;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class UserProfileCreateController extends AbstractEndpoint {

	private final UserProfileCreateService userProfileCreateService;
	private final UserProfileMappingService userProfileMappingService;
	
	public UserProfileCreateController(UserProfileCreateService userProfileCreateService,
			UserProfileMappingService userProfileMappingService) {
		this.userProfileCreateService = userProfileCreateService;
		this.userProfileMappingService = userProfileMappingService;
	}

	@RequestMapping(
            value = "/admin/-/user",
            method = RequestMethod.POST)
	public UserProfileDTO createUser(@RequestBody UserProfileCreateRequest request) 
			throws Exception {
		if(validateRequest(request)) {
	    	UserProfile userProfile = userProfileCreateService.execute(request);
	    	if(userProfile == null) {
	    		throw new RestApiException("Unable to create user profile");
	    	}
	    	return userProfileMappingService.map(userProfile);
		} 
		return null;
	}
	
	private boolean validateRequest(UserProfileCreateRequest request) throws Exception {
		if(StringUtils.isBlank(request.getEmail())) {
			throw new RestApiException("Bad request, email is null");
		} else if(StringUtils.isBlank(request.getPassword())) {
			throw new RestApiException("Bad request, password is null");
		}
		return true;
	}
}
