package com.xchain.bidder.hub.models;

import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class CommodityDTO extends AbstractModel {

	private String commodityName;
	private String commodityDescription;
	private Double price;
	private String category;
}
