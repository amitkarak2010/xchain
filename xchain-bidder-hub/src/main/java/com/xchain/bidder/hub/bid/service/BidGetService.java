package com.xchain.bidder.hub.bid.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.mappings.BidMappingService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.bidder.hub.repositories.BidRespository;
import com.xchain.commons.api.core.exceptions.NotFoundException;

@Service
public class BidGetService {

	private final BidMappingService bidMappingService;
	private final BidRespository bidRespository;
	
	public BidGetService(BidMappingService bidMappingService,
			BidRespository bidRespository) {
		this.bidMappingService = bidMappingService;
		this.bidRespository = bidRespository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BidDTO execute(BidGetRequest request) throws Exception {
		Bid bid = bidRespository.findOne(request.getBidId());
		if(bid == null) {
			throw new NotFoundException("No Bid found with id : " + request.getBidId());
		}
		return bidMappingService.map(bid);
	}
}
