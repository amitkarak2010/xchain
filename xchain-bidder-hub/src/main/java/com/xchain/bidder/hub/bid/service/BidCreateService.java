package com.xchain.bidder.hub.bid.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.mappings.BidMappingService;
import com.xchain.bidder.hub.models.BidDTO;
import com.xchain.bidder.hub.repositories.BidRespository;
import com.xchain.bidder.hub.utils.BidStatus;

@Service
public class BidCreateService {

	private final BidMappingService bidMappingService;
	private final BidRespository bidRepository;
	
	public BidCreateService(BidMappingService bidMappingService,
			BidRespository bidRepository) {
		this.bidMappingService = bidMappingService;
		this.bidRepository = bidRepository;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public BidDTO execute(BidCreateRequest request) throws Exception {
		Bid bid = bidMappingService.map(request);
		bid.setBidStatus(BidStatus.ACTIVE.toString());
		bid = bidRepository.save(bid);
		return bidMappingService.map(bid);
	}
}
