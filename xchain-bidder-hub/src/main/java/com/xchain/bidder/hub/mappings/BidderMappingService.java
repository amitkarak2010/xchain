package com.xchain.bidder.hub.mappings;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.bidder.service.BidderCreateRequest;
import com.xchain.bidder.hub.entities.Bidder;
import com.xchain.bidder.hub.models.BidderDTO;
import com.xchain.bidder.hub.models.BidderPageDTO;

@Service
public class BidderMappingService extends AbstractMappingSerice {

	public Bidder map(BidderCreateRequest request) {
		return this.modelMapper.map(request, Bidder.class);
	}
	
	public BidderDTO map(Bidder source) {
		return this.modelMapper.map(source, BidderDTO.class);
	}
	
	public BidderPageDTO map(Page<Bidder> page) {
		return this.modelMapper.map(page, BidderPageDTO.class);
	}
}
