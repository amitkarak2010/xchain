package com.xchain.bidder.hub.bid.service;

import static com.xchain.bidder.hub.repositories.BidPredicates.withBidOwner;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.mappings.BidMappingService;
import com.xchain.bidder.hub.models.BidPageDTO;
import com.xchain.bidder.hub.repositories.BidRespository;

import static com.xchain.bidder.hub.repositories.BidPredicates.withQuoteId;

@Service
public class BidMatchService {

	private final BidMappingService bidMappingService;
	private final BidRespository bidRespository;
	
	public BidMatchService(BidMappingService bidMappingService,
			BidRespository bidRespository) {
		this.bidMappingService = bidMappingService;
		this.bidRespository = bidRespository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BidPageDTO execute(BidMatchRequest request) throws Exception {
		Map<String, String> filter = request.getFilter();
		UUID quoteId = extractUUID("quoteId", filter);
		Page<Bid> bids = bidRespository.findAll(
				withQuoteId(quoteId), request.getPageable());
		return this.bidMappingService.map(bids);
	}
	
	private UUID extractUUID(String field, Map<String, String> filter) {
		String value = filter.get(field);
		if(value != null && StringUtils.isNotBlank(value)) {
			return UUID.fromString(value);
		}
		return null;
	}
}
