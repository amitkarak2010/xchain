package com.xchain.bidder.hub.quote.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.quote.service.QuoteCreateRequest;
import com.xchain.bidder.hub.quote.service.QuoteCreateService;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.BadRequestException;

@RestController
public class QuoteCreateController extends AbstractEndpoint {

	private final QuoteCreateService quoteCreateService;
	
	public QuoteCreateController(QuoteCreateService quoteCreateService) {
		this.quoteCreateService = quoteCreateService;
	}
	
	@RequestMapping(
            value = "/quote",
            method = RequestMethod.POST)
	public QuoteDTO createQuote(@RequestBody QuoteCreateRequest request) throws Exception {
		if(validateRequest(request)) {
			QuoteDTO quote = quoteCreateService.execute(request);
			return quote;
		} else {
			throw new BadRequestException("Bad quote create request");
		}
	}
	
	private Boolean validateRequest(QuoteCreateRequest request) {
		if(request.getCartId() == null) {
			LOG.error("Bad quote create request, cart id can not be null for a quote");
			return false;
		} else if(request.getQuoteName() == null 
				|| StringUtils.isBlank(request.getQuoteName())) {
			LOG.error("Bad quote create request, quote name can not be null for a quote");
			return false;
		} else if(request.getQuoteOwnerId() == null) {
			LOG.error("Bad quote create request, quote owner id can not be null for a quote");
			return false;
		} else if(request.getQuotePrice() == null) {
			LOG.error("Bad quote create request, quote price can not be null for a quote");
			return false;
		}
		return true;
	}
}
