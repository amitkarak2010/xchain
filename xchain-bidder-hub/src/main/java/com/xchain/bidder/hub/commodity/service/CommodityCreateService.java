package com.xchain.bidder.hub.commodity.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Commodity;
import com.xchain.bidder.hub.mappings.CommodityMappingService;
import com.xchain.bidder.hub.repositories.CommodityRepository;

@Service
public class CommodityCreateService {
	private final CommodityRepository commodityRepository;
	private final CommodityMappingService commodityMappingService;
	
	public CommodityCreateService(CommodityRepository commodityRepository,
			CommodityMappingService commodityMappingService) {
		this.commodityRepository = commodityRepository;
		this.commodityMappingService = commodityMappingService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public Commodity execute(CommodityCreateRequest request) throws Exception {
		Commodity commodity = commodityMappingService.map(request);
		commodity = commodityRepository.save(commodity);
		return commodity;
	}
}
