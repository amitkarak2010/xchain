package com.xchain.bidder.hub.bidder.service;

import org.springframework.data.domain.Pageable;

import lombok.Data;

@Data
public class BidderListRequest {

	private Pageable pageable;
	
	public BidderListRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
