package com.xchain.bidder.hub.quote.controllers;

import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.models.QuotePageDTO;
import com.xchain.bidder.hub.quote.service.QuoteMatchRequest;
import com.xchain.bidder.hub.quote.service.QuoteMatchService;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class QuoteMatchController extends AbstractEndpoint {

	private final QuoteMatchService quoteMatchService;
	
	public QuoteMatchController(QuoteMatchService quoteMatchService) {
		this.quoteMatchService = quoteMatchService;
	}
	
    @RequestMapping(
            value = "/quote/match",
            method = RequestMethod.GET)
    public QuotePageDTO handleRequest(@RequestParam Map<String, String> filter,
            @SortDefault.SortDefaults({
                @SortDefault(sort = "lastModified", direction = Direction.DESC)
            }) Pageable pageable) throws Exception {
    	
    	return quoteMatchService.execute(new QuoteMatchRequest()
    			.withFilter(filter).withPageable(pageable));
    }
}
