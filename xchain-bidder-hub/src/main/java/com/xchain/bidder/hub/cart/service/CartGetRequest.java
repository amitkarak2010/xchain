package com.xchain.bidder.hub.cart.service;

import java.util.UUID;

import lombok.Data;

@Data
public class CartGetRequest {

	private UUID cartId;

	public CartGetRequest withCartId(UUID cartId) {
		this.cartId = cartId;
		return this;
	}
}
