package com.xchain.bidder.hub.mappings;

import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.entities.UserProfile;
import com.xchain.bidder.hub.models.UserProfileDTO;
import com.xchain.bidder.hub.user.service.UserProfileCreateRequest;

@Service
public class UserProfileMappingService extends AbstractMappingSerice {

	public UserProfile map(UserProfileCreateRequest request) {
		return this.modelMapper.map(request, UserProfile.class);
	}
	
	public UserProfileDTO map(UserProfile source) {
		return this.modelMapper.map(source, UserProfileDTO.class);
	}
}
