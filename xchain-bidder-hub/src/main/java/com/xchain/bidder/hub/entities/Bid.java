package com.xchain.bidder.hub.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "quote")
public class Bid extends AbstractEntity {

	private static final long serialVersionUID = 3054547435635614910L;

	@NotNull
    @ManyToOne(optional = false)
    private Quote quote;
    
    @Column(name = "bid_owner_id")
	private UUID bidOwnerId;
    
	@Column(name = "bid_price")
	private Double bidPrice;
	
	@Column(name = "bid_status")
	private String bidStatus;

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

	public UUID getBidOwnerId() {
		return bidOwnerId;
	}

	public void setBidOwnerId(UUID bidOwnerId) {
		this.bidOwnerId = bidOwnerId;
	}

	public Double getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(Double bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getBidStatus() {
		return bidStatus;
	}

	public void setBidStatus(String bidStatus) {
		this.bidStatus = bidStatus;
	}
}
