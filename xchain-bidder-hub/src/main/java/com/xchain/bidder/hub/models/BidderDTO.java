package com.xchain.bidder.hub.models;

import java.util.LinkedHashSet;
import java.util.Set;

import com.xchain.bidder.hub.entities.Commodity;
import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class BidderDTO extends AbstractModel {

	private String firstName;
	private String lastName;
	private String email;
	private String contactNumber;
	private String city;
	private String state;
	private String country;
	private Set<Commodity> equipment = new LinkedHashSet<>();
}
