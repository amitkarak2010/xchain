package com.xchain.bidder.hub.bid.controllers;

import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.bid.service.BidListRequest;
import com.xchain.bidder.hub.bid.service.BidListService;
import com.xchain.bidder.hub.models.BidPageDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class BidListController extends AbstractEndpoint {

	private final BidListService bidListService;
	
	public BidListController(BidListService bidListService) {
		this.bidListService = bidListService;
	}
	
    @RequestMapping(
            value = "/bids/{bidOwnerId}/list",
            method = RequestMethod.GET)
    public BidPageDTO listBids(@PathVariable UUID bidOwnerId,
            @SortDefault.SortDefaults({
                @SortDefault(sort = "lastModified", direction = Direction.DESC)
            }) Pageable pageable) throws Exception {
    	
    	return bidListService.execute(new BidListRequest()
    			.withBidOwnerId(bidOwnerId).withPageable(pageable));
    }
}
