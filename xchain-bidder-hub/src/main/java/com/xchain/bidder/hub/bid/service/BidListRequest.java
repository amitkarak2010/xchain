package com.xchain.bidder.hub.bid.service;

import java.util.UUID;

import org.springframework.data.domain.Pageable;

import lombok.Data;

@Data
public class BidListRequest {

	private UUID bidOwnerId;
	private Pageable pageable;
	
	public BidListRequest withBidOwnerId(UUID bidOwnerId) {
		this.bidOwnerId = bidOwnerId;
		return this;
	}
	
	public BidListRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
