package com.xchain.bidder.hub.mappings;

import org.modelmapper.ModelMapper;

public abstract class AbstractMappingSerice {


	protected final ModelMapper modelMapper;
	
	public AbstractMappingSerice() {
		modelMapper = new ModelMapper();
	}
}
