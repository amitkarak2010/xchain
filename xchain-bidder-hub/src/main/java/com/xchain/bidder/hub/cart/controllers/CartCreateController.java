package com.xchain.bidder.hub.cart.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.cart.service.CartCreateRequest;
import com.xchain.bidder.hub.cart.service.CartCreateService;
import com.xchain.bidder.hub.models.CartDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.BadRequestException;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class CartCreateController extends AbstractEndpoint {

	private final CartCreateService cartCreateService;
	
	public CartCreateController(CartCreateService cartCreateService) {
		this.cartCreateService = cartCreateService;
	}

	@RequestMapping(
            value = "/cart",
            method = RequestMethod.POST)
	public CartDTO createCart(@RequestBody CartCreateRequest request) throws Exception {
		if(validateRequest(request)) {
	    	CartDTO cart = cartCreateService.execute(request);
	    	if(cart == null) {
	    		throw new RestApiException("Unable to create cart with cart name : " 
	    										+request.getCartName());
	    	}
	    	return cart;
		} else {
			throw new BadRequestException("Cart owner id can not be null");
		}
	}

	private Boolean validateRequest(CartCreateRequest request) {
		if(request.getCartOwnerId() == null) {
			return false;
		}
		return true;
	}
}
