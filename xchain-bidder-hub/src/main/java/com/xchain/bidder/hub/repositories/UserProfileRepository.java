package com.xchain.bidder.hub.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.xchain.bidder.hub.entities.UserProfile;

public interface UserProfileRepository extends JpaRepository<UserProfile, UUID>, QueryDslPredicateExecutor<UserProfile> {

}
