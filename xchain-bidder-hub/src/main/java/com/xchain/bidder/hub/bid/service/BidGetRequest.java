package com.xchain.bidder.hub.bid.service;

import java.util.UUID;

import lombok.Data;

@Data
public class BidGetRequest {

	private UUID bidId;
	
	public BidGetRequest withBidId(UUID bidId) {
		this.bidId = bidId;
		return this;
	}
}
