package com.xchain.bidder.hub.user.service;

import com.xchain.bidder.hub.models.UserProfileDTO;

public class UserProfileCreateRequest extends UserProfileDTO {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
