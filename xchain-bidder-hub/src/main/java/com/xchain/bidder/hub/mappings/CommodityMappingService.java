package com.xchain.bidder.hub.mappings;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.xchain.bidder.hub.entities.Commodity;
import com.xchain.bidder.hub.models.CommodityDTO;
import com.xchain.bidder.hub.models.CommodityPageDTO;

@Service
public class CommodityMappingService extends AbstractMappingSerice {

	public Commodity map(CommodityDTO request) {
		return this.modelMapper.map(request, Commodity.class);
	}
	
	public CommodityDTO map(Commodity source) {
		return this.modelMapper.map(source, CommodityDTO.class);
	}
	
	public CommodityPageDTO map(Page<Commodity> source) {
		return this.modelMapper.map(source, CommodityPageDTO.class);
	}
	
	public Set<Commodity> map(Set<CommodityDTO> source) {
		Set<Commodity> commodity = new LinkedHashSet<>();
		if(source == null) {
			return commodity;
		}
		Iterator<CommodityDTO> iter = source.iterator();
		while(iter.hasNext()) {
			commodity.add(map(iter.next()));
		}
		return commodity;
	}
}
