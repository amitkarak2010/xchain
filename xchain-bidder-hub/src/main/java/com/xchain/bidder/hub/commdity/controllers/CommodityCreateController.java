package com.xchain.bidder.hub.commdity.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.commodity.service.CommodityCreateRequest;
import com.xchain.bidder.hub.commodity.service.CommodityCreateService;
import com.xchain.bidder.hub.entities.Commodity;
import com.xchain.bidder.hub.mappings.CommodityMappingService;
import com.xchain.bidder.hub.models.CommodityDTO;
import com.xchain.commons.api.core.AbstractEndpoint;
import com.xchain.commons.api.core.exceptions.RestApiException;

@RestController
public class CommodityCreateController extends AbstractEndpoint {
	
	private final CommodityCreateService commodityCreateService;
	private final CommodityMappingService commodityMappingService;
	
	public CommodityCreateController(CommodityCreateService commodityCreateService,
			CommodityMappingService commodityMappingService) {
		this.commodityCreateService = commodityCreateService;
		this.commodityMappingService = commodityMappingService;
	}

	@RequestMapping(
            value = "/admin/-/commodity",
            method = RequestMethod.POST)
	public CommodityDTO createCommodity(@RequestBody CommodityCreateRequest request) throws Exception {
    	Commodity commodity = commodityCreateService.execute(request);
    	if(commodity == null) {
    		throw new RestApiException("Unable to create user profile");
    	}
    	return commodityMappingService.map(commodity);
	}

}
