package com.xchain.bidder.hub.quote.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.mappings.QuoteMappingService;
import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.repositories.QuoteRepository;
import com.xchain.commons.api.core.exceptions.NotFoundException;

@Service
public class QuoteGetService {

	private final QuoteRepository quoteRepository;
	private final QuoteMappingService quoteMappingService;
	
	public QuoteGetService(QuoteRepository quoteRepository,
			QuoteMappingService quoteMappingService) {
		this.quoteRepository = quoteRepository;
		this.quoteMappingService = quoteMappingService;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public QuoteDTO execute(QuoteGetRequest request) throws Exception {
		Quote quote = quoteRepository.findOne(request.getQuoteId());
		if(quote == null) {
			throw new NotFoundException("No quote exists with quote id : " + request.getQuoteId());
		}
		return quoteMappingService.map(quote);
	}
}
