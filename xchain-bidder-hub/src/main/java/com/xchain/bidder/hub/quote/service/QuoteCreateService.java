package com.xchain.bidder.hub.quote.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.mappings.QuoteMappingService;
import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.repositories.QuoteRepository;
import com.xchain.bidder.hub.utils.QuoteStatus;

@Service
public class QuoteCreateService {

	private final QuoteRepository quoteRepository;
	private final QuoteMappingService quoteMappingService;
	
	public QuoteCreateService(QuoteRepository quoteRepository,
			QuoteMappingService quoteMappingService) {
		this.quoteMappingService = quoteMappingService;
		this.quoteRepository = quoteRepository;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public QuoteDTO execute(QuoteCreateRequest request) throws Exception {
		Quote quote = quoteMappingService.map(request);
		quote = quoteRepository.save(quote);
		quote.setQuoteStatus(QuoteStatus.DRAFT.toString());
		return quoteMappingService.map(quote);
	}
}
