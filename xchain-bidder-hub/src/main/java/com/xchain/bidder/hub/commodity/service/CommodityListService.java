package com.xchain.bidder.hub.commodity.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Commodity;
import com.xchain.bidder.hub.mappings.CommodityMappingService;
import com.xchain.bidder.hub.models.CommodityPageDTO;
import com.xchain.bidder.hub.repositories.CommodityRepository;

@Service
public class CommodityListService {

	private final CommodityRepository commodityRepository;
	private final CommodityMappingService commodityMappingService;
	
	public CommodityListService(CommodityRepository commodityRepository,
			CommodityMappingService commodityMappingService) {
		this.commodityMappingService = commodityMappingService;
		this.commodityRepository = commodityRepository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public CommodityPageDTO execute(CommodityListRequest request) {
		Page<Commodity> commodities = commodityRepository.findAll(request.getPageable());
		return commodityMappingService.map(commodities);
	}
}
