package com.xchain.bidder.hub.cart.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Cart;
import com.xchain.bidder.hub.mappings.CartMappingService;
import com.xchain.bidder.hub.models.CartDTO;
import com.xchain.bidder.hub.repositories.CartRespository;

@Service
public class CartCreateService {

	private final CartRespository cartRepository;
	private final CartMappingService cartMappingService;
	
	public CartCreateService(CartRespository cartRepository,
			CartMappingService cartMappingService) {
		this.cartRepository = cartRepository;
		this.cartMappingService = cartMappingService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public CartDTO execute(CartCreateRequest request) throws Exception {
		Cart cart = cartMappingService.map(request);
		cart = cartRepository.save(cart);
		return cartMappingService.map(cart);
	}
}
