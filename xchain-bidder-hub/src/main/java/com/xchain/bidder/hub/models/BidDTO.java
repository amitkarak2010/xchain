package com.xchain.bidder.hub.models;

import java.util.UUID;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class BidDTO extends AbstractModel {

    private Quote quote;
	private UUID bidOwnerId;
	private Double bidPrice;
	private String bidStatus;
	private UUID quoteId;
}
