package com.xchain.bidder.hub.commdity.controllers;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.commodity.service.CommodityListRequest;
import com.xchain.bidder.hub.commodity.service.CommodityListService;
import com.xchain.bidder.hub.models.CommodityPageDTO;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class CommodityListController extends AbstractEndpoint {

	private final CommodityListService commodityListService;
	
	public CommodityListController(CommodityListService commodityListService) {
		this.commodityListService = commodityListService;
	}
	
    @RequestMapping(
            value = "/commodity/list",
            method = RequestMethod.GET)
    public CommodityPageDTO listCommodities(
    		@SortDefault.SortDefaults({
                @SortDefault(sort = "lastModified", direction = Direction.DESC)
            }) Pageable pageable) throws Exception {
    	
    	return commodityListService.execute(new CommodityListRequest()
    			.withPageable(pageable));
    }
}
