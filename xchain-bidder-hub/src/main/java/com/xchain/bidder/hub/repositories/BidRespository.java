package com.xchain.bidder.hub.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.xchain.bidder.hub.entities.Bid;

public interface BidRespository extends JpaRepository<Bid, UUID>, 
											QueryDslPredicateExecutor<Bid> {

}
