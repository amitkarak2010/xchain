package com.xchain.bidder.hub.models;

import java.util.Set;
import java.util.UUID;

import com.xchain.sdk.core.models.AbstractModel;

import lombok.Data;

@Data
public class CartDTO extends AbstractModel {
	
	private String cartName;
	private UUID cartOwnerId;
    private Set<CommodityDTO> commodityDTO;
}
