package com.xchain.bidder.hub.utils;

import org.apache.commons.lang3.StringUtils;

public enum BidStatus {

	ACTIVE("ACTIVE"),
	ACCEPT("ACCEPT"),
	DECLINE("DECLINE"),
	COUNTERED("COUNTERED"),
	EXPIRED("EXPIRED");

	private String description;

	BidStatus(String description) {
    	this.description = description;
    }

	public static BidStatus fromName(String name) {
		if (!StringUtils.isEmpty(name)) {
			name = name.trim();
			BidStatus[] bidStatus = BidStatus.values();
			for (BidStatus status : bidStatus) {
				if (status.name().equalsIgnoreCase(name) 
						|| status.description.equalsIgnoreCase(name)) {
					return status;
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.description;
	}

}
