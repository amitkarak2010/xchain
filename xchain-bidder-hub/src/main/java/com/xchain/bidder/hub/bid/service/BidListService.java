package com.xchain.bidder.hub.bid.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Bid;
import com.xchain.bidder.hub.mappings.BidMappingService;
import com.xchain.bidder.hub.models.BidPageDTO;
import com.xchain.bidder.hub.repositories.BidRespository;

import static com.xchain.bidder.hub.repositories.BidPredicates.withBidOwner;

@Service
public class BidListService {

	private final BidMappingService bidMappingService;
	private final BidRespository bidRespository;
	
	public BidListService(BidMappingService bidMappingService,
			BidRespository bidRespository) {
		this.bidMappingService = bidMappingService;
		this.bidRespository = bidRespository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BidPageDTO execute(BidListRequest request) throws Exception {
		Page<Bid> bids = bidRespository.findAll(
				withBidOwner(request.getBidOwnerId()), request.getPageable());
		return this.bidMappingService.map(bids);
	}
}
