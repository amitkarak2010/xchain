package com.xchain.bidder.hub.quote.controllers;

import java.util.UUID;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xchain.bidder.hub.models.QuoteDTO;
import com.xchain.bidder.hub.quote.service.QuoteGetRequest;
import com.xchain.bidder.hub.quote.service.QuoteGetService;
import com.xchain.commons.api.core.AbstractEndpoint;

@RestController
public class QuoteGetController extends AbstractEndpoint {

	private final QuoteGetService quoteGetService;
	
	public QuoteGetController(QuoteGetService quoteGetService) {
		this.quoteGetService = quoteGetService;
	}
	
	@RequestMapping(
            value = "/quote/{quoteId}",
            method = RequestMethod.GET)
	public QuoteDTO getQuote(@PathVariable UUID quoteId) throws Exception {
		return quoteGetService.execute(
				new QuoteGetRequest().withQuoteId(quoteId));
	}
}
