package com.xchain.bidder.hub;

import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import com.xchain.commons.spring.jwt.GenerateJWT;

/**
 * Main class to start the microservice. It will be used to generate JWT tokens as well
 * for using it with UI component or other microservices calls.
 * @author amitkumar
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.xchain"})
public class BidderHubCLI implements CommandLineRunner {

    private static final String USERNAME_OPTION = "username";

    private static final String ROLES_OPTION = "roles";

    private static final String GENERATE_JWT = "generate-jwt";
    
    private static final String HELP_OPTION = "help";

    public static void main(String[] args) throws Exception {

        Options options = configureCLIOptions();
        CommandLine cmd = parseCommandLineArguments(options, args);

        boolean printHelp = cmd.hasOption(HELP_OPTION);
        boolean generateJwt = cmd.hasOption(GENERATE_JWT);
        
        if (printHelp) {
            printHelp(options);
        } else if (generateJwt) {
            generateJWT(cmd);
        } else {
            startSpring(args);
        }
    }

    private static void startSpring(String[] args) {
        new SpringApplicationBuilder(BidderHubCLI.class)
                .build()
                .run(args);
    }

    private static void generateJWT(CommandLine cmd) throws FileNotFoundException {
        String username = cmd.getOptionValue(USERNAME_OPTION);
        String[] roles = cmd.getOptionValues(ROLES_OPTION);
        GenerateJWT.generateToken(username, roles);
        System.exit(0);
    }

    @Override
    public void run(String... args) throws Exception {
    }

    private static CommandLine parseCommandLineArguments(Options options, String[] args)
            throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    private static Options configureCLIOptions() {
        Options options = new Options();

        Option generateJwtOption = Option
                .builder()
                .argName(GENERATE_JWT)
                .longOpt(GENERATE_JWT)
                .desc("generate a JTW")
                .build();
        
        Option usernameOption = Option
                .builder()
                .argName(USERNAME_OPTION)
                .longOpt(USERNAME_OPTION)
                .hasArg()
                .desc("user to generate token for")
                .build();

        Option rolesOption = Option
                .builder()
                .argName(ROLES_OPTION)
                .longOpt(ROLES_OPTION)
                .hasArgs()
                .desc("the users roles")
                .build();

        Option helpOption = Option
                .builder("h")
                .argName(HELP_OPTION)
                .longOpt(HELP_OPTION)
                .desc("print help information")
                .build();

        options.addOption(generateJwtOption);
        options.addOption(usernameOption);
        options.addOption(rolesOption);
        options.addOption(helpOption);

        return options;
    }

    public static void printHelp(Options options) {
        final HelpFormatter formatter = new HelpFormatter();
        final String syntax = "java -jar ./build/libs/xchain-bidder-hub-0.1.0.jar";
        final String usageHeader = "where options include:";

        formatter.printHelp(syntax, usageHeader, options, null);
        System.exit(0);
    }
}
