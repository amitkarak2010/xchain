package com.xchain.bidder.hub.quote.service;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xchain.bidder.hub.entities.Quote;
import com.xchain.bidder.hub.mappings.QuoteMappingService;
import com.xchain.bidder.hub.models.QuotePageDTO;
import com.xchain.bidder.hub.repositories.QuoteRepository;

import static com.xchain.bidder.hub.repositories.QuotePredicates.withQuoteOwner;

@Service
public class QuoteMatchService {

	private final QuoteMappingService quoteMappingService;
	private final QuoteRepository quoteRepository;
	
	public QuoteMatchService(QuoteMappingService quoteMappingService,
			QuoteRepository quoteRepository) {
		this.quoteMappingService = quoteMappingService;
		this.quoteRepository = quoteRepository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public QuotePageDTO execute(QuoteMatchRequest request) throws Exception {
		Map<String, String> filter = request.getFilter();
		UUID quoteOwnerId = extractUUID("quoteOwnerId", filter);
		Page<Quote> quotes = quoteRepository.findAll(withQuoteOwner(quoteOwnerId), 
				request.getPageable());
		return quoteMappingService.map(quotes);
	}
	
	private UUID extractUUID(String field, Map<String, String> filter) {
		String value = filter.get(field);
		if(value != null && StringUtils.isNotBlank(value)) {
			return UUID.fromString(value);
		}
		return null;
	}
}
