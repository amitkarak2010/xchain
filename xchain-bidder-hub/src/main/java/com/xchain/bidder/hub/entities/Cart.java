package com.xchain.bidder.hub.entities;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart extends AbstractEntity {

	private static final long serialVersionUID = 6413623008694351074L;

	@Column(name = "cart_name")
	private String cartName;
	
	@Column(name = "cart_owner_id")
	private UUID cartOwnerId;
	
	@ManyToMany(fetch = FetchType.EAGER)
    private Set<Commodity> commodity = new LinkedHashSet<>();

	public String getCartName() {
		return cartName;
	}

	public void setCartName(String cartName) {
		this.cartName = cartName;
	}

	public UUID getCartOwnerId() {
		return cartOwnerId;
	}

	public void setCartOwnerId(UUID cartOwnerId) {
		this.cartOwnerId = cartOwnerId;
	}

	public Set<Commodity> getCommodity() {
		return commodity;
	}

	public void setCommodity(Set<Commodity> commodity) {
		this.commodity = commodity;
	}
}
