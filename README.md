# xchain-bidder-hub
  - The repository contains the code for an assignement given by a startup company called xchain.
  - The services are exposed as part of spring boot application and below were the use cases details for the assignements:-
  # Requirements
    - A bidding environment where an End User can create the bookings of the items and create quote.
    - A bidding environment where a Bidder User type can bid for the quote.
    - The bidding platform should be able to show the bids for the quotes which are leading.
    - The bidding platform should be able to facilitate user login and restrict the api uses.
    - The bidding platform should be able to facilitate creation of carts and addtion of "n" items.
    - The bidding platform should facilitate a Bidder type user to accept/decline the bid or change the bid price in real time.
    - The bidding platform should facilitate Quote expiration time.
  
  PS: The repository contains the code for exposing rest services and all the building components and abstract layer

# Implementation Details
- backend code implemented with following modules
  - xchain-commons
    - Commons module to have the jwt based classes
    - api core for abstract classes

  - xchain-sdk
    - abstract and general utility classes for models and paginations
    - need to abstract all the models to respective sdk's

  - xchain-bidder-hub
    - bidder hub code to expose all the rest api's.

# TODO
  - Implement the UI on top of rest apis for complete life cycle of bidding and leader board.
  - Integrate the backend with websockets for realtime bidding updates on leader board.
  - Testing and documentation of APIs.
  -Integration test with UI and websocket

  
