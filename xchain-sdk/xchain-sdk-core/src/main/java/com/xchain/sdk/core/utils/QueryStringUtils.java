package com.xchain.sdk.core.utils;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.web.util.UriComponentsBuilder;

public class QueryStringUtils {

    public static void addParams(
            UriComponentsBuilder builder,
            Map<String, ?> params) {

        for (Entry<String, ?> param : params.entrySet()) {
            builder.queryParam(param.getKey(), param.getValue());
        }

    }

}
