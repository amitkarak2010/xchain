package com.xchain.sdk.core.utils;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.util.UriComponentsBuilder;

public class PageableUtils {

    public static void addPageableParams(
            UriComponentsBuilder builder,
            Pageable pageable) {

        builder.queryParam("page", pageable.getPageNumber());
        builder.queryParam("size", pageable.getPageSize());


        Sort sort = pageable.getSort();
        if (sort != null) {
            for (Sort.Order order : sort) {
                String sortStr =
                        String.format("%s,%s", order.getProperty(), order.getDirection().name());
                builder.queryParam("sort", sortStr);
            }
        }
    }

}
